## Kulcs-érték paraméterek alapján a legjobban illeszkedő szolgáltatás meghatározása

Az alábbi feladatra egy rövid kódot szeretnénk kapni. Bármilyen nyelven lehet, akár pszeudokód is.

Adott egy szolgáltatás lista, amihez kulcs-érték párok tartoznak. Feladat a beérkező kulcs-érték pár inputok alapján a legjobban illeszkedő szolgáltatás megtalálása. 

Szabályrendszer:
1. A szolgáltatás megfeleltetésben lévő kód-érték párokon kívül tetszőleges kód-érték párok érkezhetnek még
1. Egy kód egyszer érkezik az inputban (tehát nem lehet pl. két typeOfCoach kód az inputon, eltérő vagy azonos értékekkel)
1. Az adott szolgáltatás kiválasztásához az összes hozzá rendelt kód-érték párnak meg kell érkeznie az inputban
1. Ha a fenti szabályok alapján több szolgáltatás is meghatározásra kerül, akkor a specifikusabbat kell választani (amelyikhez több paraméter tartozik).

| Kód | Érték |
| ------ | ------ |
| specialFeatures | 54 |
| typeOfCoach | 52 |
| class | 2 |
| typeOfReservation | 2 |

**Elvárt output:** Fekvőhely 2. osztály 5 személyes fülkében (D5) <br>
**Magyarázat:** TypeOfCoach=52 nem került figyelembe vételre<br>

| Kód | Érték |
| ------ | ------ |
| typeOfCoach | 32 |
| class | 2 |
| typeOfReservation | 1 |
| specialFeatures | 132 |

**Elvárt output:** Kerékpár<br>
**Magyarázat:** Bár az inputra az Ülőhely 2. osztály is megfelel (typeOfCoach paraméter figyelmen kívül hagyásával), de mivel a Kerékpár specifikusabb (3 paraméter felel meg az inputnak), az kerül kiválasztásra.
