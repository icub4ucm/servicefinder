package services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static services.ServiceUtil.bestServiceMatchForParams;

import java.util.AbstractMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

class ServiceUtilTest {

	private static final String TYPE_OF_RESERVATION = "typeOfReservation";
	private static final String TYPE_OF_COACH = "typeOfCoach";
	private static final String CLASS = "class";
	private static final String SPECIAL_FEATURES = "specialFeatures";
	private static final String BIKE = "Kerékpár";
	private static final String D5 = "Fekvőhely 2. osztály 5 személyes fülkében (D5)";

	@Test
	void testBestServiceMatchForParams_findD5() {
		Map<String, Integer> params = Stream.of(//
				new AbstractMap.SimpleEntry<>(SPECIAL_FEATURES, 54), //
				new AbstractMap.SimpleEntry<>(CLASS, 2), //
				new AbstractMap.SimpleEntry<>(TYPE_OF_COACH, 52), //
				new AbstractMap.SimpleEntry<>(TYPE_OF_RESERVATION, 2))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

		String actual = bestServiceMatchForParams(params);

		String expected = D5;

		assertEquals(expected, actual);
	}

	@Test
	void testBestServiceMatchForParams_findBike() {
		Map<String, Integer> params = Stream.of(//
				new AbstractMap.SimpleEntry<>(CLASS, 2), //
				new AbstractMap.SimpleEntry<>(TYPE_OF_COACH, 32), //
				new AbstractMap.SimpleEntry<>(TYPE_OF_RESERVATION, 1),
				new AbstractMap.SimpleEntry<>(SPECIAL_FEATURES, 132)) //
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

		String actual = bestServiceMatchForParams(params);

		String expected = BIKE;

		assertEquals(expected, actual);
	}

	@Test
	void testBestServiceMatchForParams_findNone() {
		Map<String, Integer> params = Stream.of(//
				new AbstractMap.SimpleEntry<>(CLASS, 333), //
				new AbstractMap.SimpleEntry<>(TYPE_OF_COACH, 333), //
				new AbstractMap.SimpleEntry<>(TYPE_OF_RESERVATION, 333),
				new AbstractMap.SimpleEntry<>(SPECIAL_FEATURES, 333)) //
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

		String actual = bestServiceMatchForParams(params);

		String expected = null;

		assertEquals(expected, actual);
	}
}
