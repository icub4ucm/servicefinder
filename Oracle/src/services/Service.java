package services;

import java.util.HashMap;
import java.util.Map;

public class Service {

	String name;

	Map<String, Integer> properties = new HashMap<>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Map<String, Integer> getProperties() {
		return properties;
	}

	public static Service createService(String name, Map<String, Integer> properties) {
		Service service = new Service();
		service.setName(name);
		service.properties.putAll(properties);
		return service;
	}
}
