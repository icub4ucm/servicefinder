package services;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

public class ServiceUtil {

	public static String bestServiceMatchForParams(Map<String, Integer> params) {

		Map<String, Integer> paramsCopy = new HashMap<>(params);

		validateParams(paramsCopy);

		Optional<Service> foundServiceOptional = Services.getServiceList().stream()
				.filter(service -> service.properties.size() == paramsCopy.size())
				.filter(service -> paramsCopy.equals(service.properties)).findFirst();

		if (foundServiceOptional.isPresent()) {
			return foundServiceOptional.get().name;
		}

		return null; // eventually throw a NotFoundException
	}

	private static void validateParams(Map<String, Integer> paramsCopy) {
		for (Iterator<Entry<String, Integer>> i = paramsCopy.entrySet().iterator(); i.hasNext();) {
			Entry<String, Integer> entry = i.next();
			if (Services.getServiceList().stream().noneMatch(service -> service.properties.get(entry.getKey()) != null
					&& paramsCopy.get(entry.getKey()).equals(service.properties.get(entry.getKey())))) {
				i.remove();
			}
		}
	}
}
