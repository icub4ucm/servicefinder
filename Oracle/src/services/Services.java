package services;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Services {

	private static final List<Service> list = new ArrayList<>();

	static {
		list.add(Service.createService("Fekvőhely 2. osztály 5 személyes fülkében (D5)", //
				Stream.of(new AbstractMap.SimpleEntry<>("specialFeatures", 54), //
						new AbstractMap.SimpleEntry<>("class", 2), //
						new AbstractMap.SimpleEntry<>("typeOfReservation", 2))
						.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue))));

		list.add(Service.createService("Kerékpár", Stream.of(//
				new AbstractMap.SimpleEntry<>("typeOfCoach", 32), //
				new AbstractMap.SimpleEntry<>("class", 2), //
				new AbstractMap.SimpleEntry<>("typeOfReservation", 1))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue))));

		list.add(Service.createService("Tandem", Stream.of(//
				new AbstractMap.SimpleEntry<>("typeOfCoach", 35), //
				new AbstractMap.SimpleEntry<>("class", 2), //
				new AbstractMap.SimpleEntry<>("typeOfReservation", 1))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue))));

		list.add(Service.createService("Business Class 1. osztály", Stream.of(//
				new AbstractMap.SimpleEntry<>("typeOfCoach", 21), //
				new AbstractMap.SimpleEntry<>("class", 1), //
				new AbstractMap.SimpleEntry<>("typeOfReservation", 1))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue))));

		list.add(Service.createService("Ülőhely 1. osztály", Stream.of(//
				new AbstractMap.SimpleEntry<>("class", 1), //
				new AbstractMap.SimpleEntry<>("typeOfReservation", 1))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue))));

		list.add(Service.createService("Ülőhely 2. osztály", Stream.of(//
				new AbstractMap.SimpleEntry<>("class", 2), //
				new AbstractMap.SimpleEntry<>("typeOfReservation", 1))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue))));
	}

	public static List<Service> getServiceList() {
		return list;
	}
}
